module neetsdkasu.Parser

type Source = string
type StartPos = int
type NextPos = int
type Word = string

type ITokenKey =
    interface
    end

type ITokenContainer =
    interface
        abstract member Add : ITokenKey * Word -> ITokenContainer option
    end

type State = Source * StartPos * NextPos * ITokenContainer
type ParseResult = State option

type IParser =
    interface
        abstract member Parse : State -> ParseResult
    end


module ParserUtil =
    let parse (parser : IParser) (src : State) : ParseResult = parser.Parse(src)


module TokenContainerUtil =
    let add (key : ITokenKey) (word : Word) (container : ITokenContainer) : ITokenContainer option =
        container.Add(key, word)


module StateUtil =

    let inline make (src : string) (spos : int) (npos : int) (tokens : ITokenContainer) : State =
        (src, spos, npos, tokens)

    let inline init (src : string) (pos : int) (tokens : ITokenContainer) : State =
        make src pos pos tokens

    let inline src (state : State) : string =
        let (src, _, _, _) = state
        src

    let inline spos (state : State) : int =
        let (_, spos, _, _) = state
        spos

    let inline npos (state: State) : int =
        let (_, _, npos, _) = state
        npos

    let inline tokens (state: State) : ITokenContainer =
        let (_, _, _, tokens) = state
        tokens

    let inline replaceTokens (state : State) (tokens : ITokenContainer) : State =
        make (src state) (spos state) (npos state) tokens

    let inline replacePos (state : State) (spos : int) (npos : int) : State =
        make (src state) spos npos (tokens state)

    let inline replaceSpos (state : State) (spos : int) : State =
        make (src state) spos (npos state) (tokens state)

    let inline srcLength (state : State) : int =
        String.length (src state)

    let inline srcSubstring (state : State) (pos : int) (len : int) : string =
        (src state).Substring(pos, len)

    let inline srcChar (state : State) (pos : int) : char =
        (src state).[pos]

    let inline length (state : State) : int =
        npos state - spos state

    let inline remain (state : State) : int =
        max 0 (srcLength state - npos state)

    let inline getRemain (state : State) : string =
        srcSubstring state (npos state) (remain state)

    let getWord (state : State) : Word option =
        let spos = spos state
        let npos = npos state
        let len = srcLength state
        if spos >= len || npos > len then
            None
        else
            Some (srcSubstring state spos (npos - spos))

    let getStr (state : State) (len : int) : (string * State) option =
        let pos = npos state
        if pos + len > srcLength state then
            None
        else
            let str = srcSubstring state pos len
            Some (str, replacePos state pos (pos + len))

    let getChar (state : State) : (char * State) option =
        let pos = npos state
        if pos + 1 > srcLength state then
            None
        else
            let ch = srcChar state pos
            Some (ch, replacePos state pos (pos + 1))

    let merge (state1 : State) (state2 : State) : State =
        replaceSpos state2 (spos state1)

    let skip (state : State) (cnt: int) : State =
        let npos = min (npos state + cnt) (srcLength state)
        replacePos state npos npos

    let appendToken (key : ITokenKey) (state : State) : State option =
        getWord state
        |> Option.bind (fun word ->
            TokenContainerUtil.add key word (tokens state)
            |> Option.map (replaceTokens state)
        )


module ParseFunc =

    let parseFromCharList (targets : char array) (src : State) : ParseResult =
        StateUtil.getChar src
        |> Option.bind (fun (ch, src) ->
            if Array.contains ch targets then
                Some src
            else
                None
        )


    let parseConstString (target : string) (src : State) : ParseResult =
        StateUtil.getStr src (String.length target)
        |> Option.bind (fun (str, src) ->
            if str = target then
                Some src
            else
                None
        )


    let parseFromCharCodeRange (under : char) (upper : char) (src : State) : ParseResult =
        StateUtil.getChar src
        |> Option.bind (fun (ch, src) ->
            if under <= ch && ch <= upper then
                Some src
            else
                None
        )


    let parseRepeat (parser : IParser) (under : int) (upper : int) (src : State) : ParseResult =
        Seq.unfold (fun (cnt, src) ->
            if cnt = upper then
                None
            else
                match ParserUtil.parse parser src with
                | None -> None
                | Some src -> Some (src, (cnt + 1 , src))
        ) (0, src)
        |> Seq.toArray
        |> (fun arr ->
            let len = Array.length arr
            if len < under then
                None
            elif len = 0 then
                Some (StateUtil.skip src 0)
            else
                let head = Array.head arr
                let last = Array.last arr
                Some (StateUtil.merge head last)
        )


    let parseAnyone (parsers : IParser array) (src : State) : ParseResult =
        Array.fold (fun res parser ->
            match (res, ParserUtil.parse parser src) with
            | (None, res) -> res
            | (res, None) -> res
            | (Some res1, Some res2) ->
                if StateUtil.length res2 > StateUtil.length res1 then
                    Some res2
                else
                    Some res1
        ) None parsers


    let parseAndConnect (parsers : IParser array) (src : State) : ParseResult =
        Array.fold (fun res parser ->
            Option.bind (fun src ->
                ParserUtil.parse parser src
                |> Option.map (StateUtil.merge src)
            ) res
        ) (Some (StateUtil.skip src 0)) parsers


    let parseToken (parser: IParser) (key : ITokenKey) (src : State) : ParseResult =
        ParserUtil.parse parser src
        |> Option.bind (StateUtil.appendToken key)


    let parseFirstMatch (parsers : IParser array) (src : State) : ParseResult =
        Array.fold (fun res parser ->
            match res with
            | None -> ParserUtil.parse parser src
            | _ -> res
        ) None parsers


    let parseCheckOnly (parser : IParser) (src : State) : ParseResult =
        match ParserUtil.parse parser src with
        | Some _-> Some (StateUtil.skip src 0)
        | _ -> None


    let parseNot (parser : IParser) (src : State) : ParseResult =
        match ParserUtil.parse parser src with
        | None -> Some (StateUtil.skip src 0)
        | _ -> None


    let parseSkip (skip : int) (src : State) : ParseResult =
        match StateUtil.remain src with
        | 0 -> None
        | r when r < skip -> None
        | _ -> Some (StateUtil.skip src skip)


    let parseChar (target : char) (src : State) : ParseResult =
        StateUtil.getChar src
        |> Option.bind (fun (ch, src) ->
            if ch = target then
                Some src
            else
                None
        )

    let parseByFunc (f : State -> NextPos option) (src : State) : ParseResult =
        f src
        |> Option.bind (fun pos ->
            let npos = StateUtil.npos src
            if npos <= pos && pos <= StateUtil.length src then
                Some (StateUtil.replacePos src npos pos)
            else
                None
        )




type CharListParser(targets : char array) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseFromCharList targets src


type ConstStringParser(target : string) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseConstString target src


type CharCodeRangeParser(under : char, upper : char) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseFromCharCodeRange under upper src


type RepeatParser(parser : IParser, under : int, upper : int) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseRepeat parser under upper src


type AnyoneParser(_parsers : IParser array) =
    let mutable parsers = _parsers
    member this.Add(parser) =
        parsers <- Array.append parsers [|parser|]
    interface IParser with
        member this.Parse(src) = ParseFunc.parseAnyone parsers src


type ConnectedParser(parsers : IParser array) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseAndConnect parsers src


type TokenParser(key : ITokenKey, parser : IParser) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseToken parser key src


type FirstMatchParser(_parsers : IParser array) =
    let mutable parsers = _parsers
    member this.Add(parser) =
        parsers <- Array.append parsers [|parser|]
    interface IParser with
        member this.Parse(src) = ParseFunc.parseFirstMatch parsers src


type CheckParser(parser : IParser) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseCheckOnly parser src


type NotParser(parser : IParser) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseNot parser src


type SkipParser(skip : int) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseSkip skip src


type CharParser(ch : char) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseChar ch src


type FuncParser(f : State -> NextPos option) =
    interface IParser with
        member this.Parse(src) = ParseFunc.parseByFunc f src


let SrcHeadParser =
    new FuncParser(fun src ->
        if StateUtil.npos src = 0 then
            Some 0
        else
            None
    )


let SrcEndParser =
    new FuncParser(fun src ->
        if StateUtil.remain src = 0 then
            Some (StateUtil.npos src)
        else
            None
    )


module Test

open neetsdkasu.Parser

type KeyType =
    | Number
    | Operator1
    | Operator2
    | EndOfLine
    | OpenBracket
    | CloseBracket
    | Sign

type TestKey(keyType : KeyType) =
    interface ITokenKey
    member this.KeyType = keyType

let NumKey = new TestKey(KeyType.Number) :> ITokenKey
let Op1Key = new TestKey(KeyType.Operator1) :> ITokenKey
let Op2Key = new TestKey(KeyType.Operator2) :> ITokenKey
let EolKey = new TestKey(KeyType.EndOfLine) :> ITokenKey
let OpBrKey = new TestKey(KeyType.OpenBracket) :> ITokenKey
let ClBrKey = new TestKey(KeyType.CloseBracket) :> ITokenKey
let SignKey = new TestKey(KeyType.Sign) :> ITokenKey


let calc ns op =
    match ns with
    | n2 :: n1 :: ns ->
        match op with
        | "+" -> Some ((n1 + n2) :: ns)
        | "-" -> Some ((n1 - n2) :: ns)
        | "*" -> Some ((n1 * n2) :: ns)
        | "/" -> Some ((n1 / n2) :: ns)
        | "(" -> Some (n2 :: n1 :: ns)
        | _ -> None
    | _ -> None

let applySign ns op =
    match ns with
    | num :: ns ->
        match op with
        | "+" -> Some (num :: ns)
        | "-" -> Some (-num :: ns)
        | _ -> None
    | _ -> None

let calcAll os ns =
    List.fold (fun res (key, op) ->
        Option.bind (fun ns ->
            match key with
            | Sign -> applySign ns op
            | _ -> calc ns op
        ) res
    ) (Some ns) os


let getCalcOps os =
    let isNotOpbr (key, _) = key <> OpenBracket
    let cops = List.takeWhile isNotOpbr os
    let nops = List.skipWhile isNotOpbr os
    cops, nops


let stackOp1 op os ns =
    let os1, os2 = getCalcOps os
    calcAll os1 ns
    |> Option.map (fun ns ->
        (KeyType.Operator1, op) :: os2, ns
    )


let stackOp2 op os ns =
    match os with
    | (Operator2, op2) :: os ->
        calc ns op2
        |> Option.map (fun ns ->
            (Operator2, op) :: os, ns
        )
    | _ ->
        Some ((Operator2, op) :: os, ns)


let openBracket op os ns =
    Some ((OpenBracket, op) :: os, ns)

let closeBracket _ os ns =
    let os1, os2 = getCalcOps os
    calcAll os1 ns
    |> Option.map (fun ns -> List.tail os2, ns)

let calcEol _ os ns =
    calcAll os ns
    |> Option.map (fun ns -> [], ns)

let stackSign op os ns =
    Some ((Sign, op) :: os, ns)

let stackNum num os ns =
    let ns = int num :: ns
    match os with
    | (Sign, op) :: os ->
        applySign ns op
        |> Option.map (fun ns -> os, ns)
    | _ -> Some (os, ns)

let getFunc (key : ITokenKey) =
    match (key :?> TestKey).KeyType with
    | Number -> stackNum
    | Operator1 -> stackOp1
    | Operator2 -> stackOp2
    | EndOfLine -> calcEol
    | OpenBracket -> openBracket
    | CloseBracket -> closeBracket
    | Sign -> stackSign


type TestContainer(opStack : (KeyType * Word) list, numStack : int list) =
    new() = TestContainer([], [])
    member this.HeadNumber =
        match numStack with
        | n :: _ -> Some n
        | _ -> None
    override this.ToString() = sprintf "TestContainer %A %A" opStack numStack
    interface ITokenContainer with
        member this.Add(key, word) =
            getFunc key word opStack numStack
            |> Option.map (fun (os, ns) ->
                new TestContainer(os, ns) :> ITokenContainer
            )



let limit = 10000


let (SP0, SP1) =
    let sp = new CharListParser(" \t".ToCharArray())
    let sp1 = new RepeatParser(sp, 0, limit)
    let sp2 = new RepeatParser(sp, 1, limit)
    sp1 :> IParser, sp2 :> IParser


let EOL =
    let eol = new CharListParser([|';'|])
    new TokenParser(EolKey, eol) :> IParser


let NUM =
    let num = new CharCodeRangeParser('0', '9')
    let num = new RepeatParser(num, 1, 9)
    new TokenParser(NumKey, num) :> IParser


let OP1 =
    let op1 = new CharListParser("+-".ToCharArray())
    new TokenParser(Op1Key, op1) :> IParser


let OP2 =
    let op2 = new CharListParser("*/".ToCharArray())
    new TokenParser(Op2Key, op2) :> IParser


let OPBR =
    let opbr = new CharListParser([|'('|])
    new TokenParser(OpBrKey, opbr) :> IParser

let CLBR =
    let clbr = new CharListParser([|')'|])
    new TokenParser(ClBrKey, clbr) :> IParser

let SIGN =
    let sign = new CharListParser("+-".ToCharArray())
    let sign = new TokenParser(SignKey, sign)
    new RepeatParser(sign, 0, 1) :> IParser

let innerfact =
    let fact = new ConnectedParser([|SP0; NUM|])
    new AnyoneParser([|fact|])
let FACT =
    new ConnectedParser([|SP0; SIGN; innerfact|]) :> IParser
let TERM =
    let term = new ConnectedParser([|SP0; OP2; FACT|])
    let term = new RepeatParser(term, 0, limit)
    new ConnectedParser([|FACT; term|]) :> IParser
let EXPR =
    let expr = new ConnectedParser([|SP0; OP1; TERM|])
    let expr = new RepeatParser(expr, 0, limit)
    new ConnectedParser([|TERM; expr|]) :> IParser
do
    let fact = new ConnectedParser([|SP0; OPBR; EXPR; SP0; CLBR|])
    innerfact.Add(fact)


let STATEMENT =
    let expr = new RepeatParser(EXPR, 0, 1)
    new ConnectedParser([|expr; SP0; EOL|]) :> IParser


let run str =
    let src = StateUtil.init str 0 (new TestContainer())
    ParserUtil.parse STATEMENT src


let get st =
    match (StateUtil.tokens st :?> TestContainer).HeadNumber with
    | None ->
        None, StateUtil.srcSubstring st (StateUtil.npos st) (StateUtil.remain st)
    | Some num ->
        if StateUtil.remain st = 0 then
            Some num, ""
        else
            Some num, StateUtil.srcSubstring st (StateUtil.npos st) (StateUtil.remain st)


let test() =
    let str = "123 + 45 * 2 - 5 / (2 - 3) + -(10 * -7) * (-3);"
    run str
    |> printfn "%A"


let rec reprun str =
    match run str with
    | None -> str
    | Some res ->
        let (num, str) = get res
        Option.iter (printfn "result: %d") num
        reprun str

let rec repl str =
    printf "%c " (if str = "" then '>' else '|')
    stdout.Flush()
    let line = stdin.ReadLine()
    match line with
    | "exit" | "quit" | "bye" -> ()
    | "clear" | "cls" | "reset" -> repl ""
    | _ ->
        let str = reprun (str + line)
        repl str

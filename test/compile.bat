@pushd %~dp0

fsc ^
    --nologo ^
    --target:exe ^
    --debug+ ^
    --debug:full ^
    --warnaserror+ ^
    --codepage:65001 ^
    ..\src\neetsdkasu.Parser.fs ^
    Test.fs ^
    Test2.fs ^
    Main.fs

@popd
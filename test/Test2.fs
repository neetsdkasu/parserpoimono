module Test2

open neetsdkasu.Parser

type MyKey(name : string) =
    interface ITokenKey
    override this.ToString() = "MyKey " + name


type MyContainer(tokens : (ITokenKey * Word) list) =
    new() = MyContainer([])
    override this.ToString() = sprintf "MyContainer %A" tokens
    interface ITokenContainer with
        member this.Add(key, word) =
            Some (new MyContainer((key, word) :: tokens) :> ITokenContainer)



let test() =
    let src = StateUtil.init "abcdefghijklm" 0 (new MyContainer())
    let p1 = new ConstStringParser("abc")
    let st = ParserUtil.parse p1 src
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let p = new CharListParser("cde".ToCharArray())
    let p2 = new RepeatParser(p, 1, 2)
    let st = ParserUtil.parse p2 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let p = new CharListParser("abcde".ToCharArray())
    let p3 = new RepeatParser(p, 0, 6)
    let st = ParserUtil.parse p3 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let p = new CharCodeRangeParser('a', 'k')
    let p4 = new RepeatParser(p, 1, 4)
    let st = ParserUtil.parse p4 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let p5 = new AnyoneParser([|p1; p2; p3; p4|])
    let st = ParserUtil.parse p5 src
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let p6 = new AnyoneParser([|p1; p2; p4|])
    let st = ParserUtil.parse p6 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)
    let st = ParserUtil.parse p6 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)
    let st = ParserUtil.parse p6 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let ps = [| new TokenParser(new MyKey("Foo"), p1)
              ; new TokenParser(new MyKey("Bar"), p2)
              ; new TokenParser(new MyKey("BAZ"), p3)
              ; new TokenParser(new MyKey("HOGE"), p4)
              |] : IParser array
    let p7 = new AnyoneParser(ps)
    let st = ParserUtil.parse p7 src
    printfn "%A %A" st (Option.bind StateUtil.getWord st)
    let st = ParserUtil.parse p7 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)
    let st = ParserUtil.parse p7 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)
    let st = ParserUtil.parse p7 (Option.get st)
    printfn "%A %A" st (Option.bind StateUtil.getWord st)

    let p8 = new ConnectedParser(ps)
    let st = ParserUtil.parse p8 src
    printfn "%A %A" st (Option.bind StateUtil.getWord st)